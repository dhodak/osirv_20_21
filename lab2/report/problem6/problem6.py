import cv2 as cv
import numpy as np

def noise(image, d):
    n = np.random.uniform(0,1, image.shape)
    image = image.astype(np.float32)
    image = (np.floor((image/d) + n) + 0.5) * d
    image[image > 255] = 255
    image[image < 0] = 0
    image = image.astype(np.uint8)
    return image

image = cv.imread("slike/BoatsColor.bmp", 0)
image = np.array(image)

for q in range(1, 9):
    d = 2**(8-q)
    qimage = noise(image, d)
    cv.imwrite("report/problem6/boat_" + str(q) + "n.bmp", qimage)


#In first four images where q has values 8 to 5, have no visible noise. 
#At q=4 there is some noise, which is visible mostly on clouds.
#With lower q values noise is more noticeable.