import numpy as np
import cv2 as cv

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)

    return output 


image = cv.imread("D:/Dev\OSIRV/osirv_20_21/lab2/slike/baboon.bmp", 0)

edgeDetection1 = np.array(([1, 0, -1], [0, 0, 0], [-1, 0, 1]))
edgeDetection2 = np.array(([0, -1, 0], [-1, 4, -1], [0, -1, 0]))
edgeDetection3 = np.array(([-1, -1, -1], [-1, 8, -1], [-1, -1, -1]))
idenity = np.array(([0, 0, 0], [0, 1, 0], [0, 0, 0]))
sharpen = np.array(([0, -1, 0], [-1, 5, -1], [0, -1, 0]))
boxBlur = np.ones((3,3)) * (1/9)
gaussianBlur = np.array(([1, 2, 1], [2, 4, 2], [1, 2, 1])) * (1/16)

edgeDetectionImage1 = convolve(image, edgeDetection1)
edgeDetectionImage2 = convolve(image, edgeDetection2)
edgeDetectionImage3 = convolve(image, edgeDetection3)
idenityImage = convolve(image, idenity)
sharpenImage = convolve(image, sharpen)
boxBlurImage = convolve(image, boxBlur)
gaussianBlurImage = convolve(image, gaussianBlur)

cv.imwrite("report/problem2/edgeDetectionImage1.bmp", edgeDetectionImage1)
cv.imwrite("report/problem2/edgeDetectionImage2.bmp", edgeDetectionImage2)
cv.imwrite("report/problem2/edgeDetectionImage3.bmp", edgeDetectionImage3)
cv.imwrite("report/problem2/idenityImage.bmp", idenityImage)
cv.imwrite("report/problem2/sharpenImage.bmp", sharpenImage)
cv.imwrite("report/problem2/boxBlurImage.bmp", boxBlurImage)
cv.imwrite("report/problem2/gaussianBlurImage.bmp", gaussianBlurImage)



