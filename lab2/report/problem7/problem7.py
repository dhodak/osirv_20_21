import cv2 as cv
import numpy as np

image = cv.imread('slike/baboon.bmp', 0)
image = cv.resize(image,None,fx=0.25, fy=0.25)
rows,cols = image.shape

rotatedImage = image.copy()
for i in range(1, 12):  
    M = cv.getRotationMatrix2D((cols/2,rows/2),30 * i,1) 
    dst = cv.warpAffine(image,M,(cols,rows))
    rotatedImage = np.hstack((rotatedImage, dst))

cv.imwrite("report/problem7/rotatedBaboon.bmp", rotatedImage)