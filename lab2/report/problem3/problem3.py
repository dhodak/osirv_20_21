import os
import cv2 as cv

images = os.listdir("D:/Dev/OSIRV/osirv_20_21/lab2/slike")

for imageName in images:
    imageName = imageName.split(".")[0]
    image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/" + imageName + ".bmp", 0)
    cv.imwrite("report/problem3/" + imageName + "_invert.bmp", cv.bitwise_not(image))


