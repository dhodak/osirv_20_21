### Problem 1

**Spajanje slika i dodavanje obruba**

```python
import numpy as np
import cv2 as cv

def makeImageBorder(bordersize, image):

    height = image.shape[0]
    width = image.shape[1]

    borderImage = np.hstack((np.zeros((height, bordersize, 3), dtype = np.uint8), image, np.zeros((height, bordersize, 3), dtype = np.uint8)))
    
    borderImage = np.vstack((np.zeros((bordersize, width + 2*bordersize, 3), dtype=np.uint8), borderImage, np.zeros((bordersize, width + 2*bordersize, 3), dtype = np.uint8)))

    return borderImage


imageWidth = 300
imageHeight = 300

image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/baboon.bmp")
image1 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/baboon.bmp")
image2 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/lenna.bmp")
image3 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/airplane.bmp")

rImage1 = cv.resize(image1, (imageWidth, imageHeight))
rImage2 = cv.resize(image2, (imageWidth, imageHeight))
rImage2 = cv.resize(image3, (imageWidth, imageHeight))

mergedImage = np.hstack((rImage1, rImage2, rImage2))

borderImage = makeImageBorder(20, image)

cv.imshow("Merged image", mergedImage)
cv.imshow("Image with border", borderImage)
cv.imwrite("report\problem1\merged.bmp", mergedImage)
cv.imwrite("report\problem1\\border.bmp", borderImage)
cv.waitKey(0)
cv.destroyAllWindows()
```

![Spojene slike](problem1/merged.bmp)
![Obrub slike](problem1/border.bmp)

### Problem 2

**Konvolucija**

```python
import numpy as np
import cv2 as cv

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)

    return output 


image = cv.imread("D:/Dev\OSIRV/osirv_20_21/lab2/slike/baboon.bmp", 0)

edgeDetection1 = np.array(([1, 0, -1], [0, 0, 0], [-1, 0, 1]))
edgeDetection2 = np.array(([0, -1, 0], [-1, 4, -1], [0, -1, 0]))
edgeDetection3 = np.array(([-1, -1, -1], [-1, 8, -1], [-1, -1, -1]))
idenity = np.array(([0, 0, 0], [0, 1, 0], [0, 0, 0]))
sharpen = np.array(([0, -1, 0], [-1, 5, -1], [0, -1, 0]))
boxBlur = np.ones((3,3)) * (1/9)
gaussianBlur = np.array(([1, 2, 1], [2, 4, 2], [1, 2, 1])) * (1/16)

edgeDetectionImage1 = convolve(image, edgeDetection1)
edgeDetectionImage2 = convolve(image, edgeDetection2)
edgeDetectionImage3 = convolve(image, edgeDetection3)
idenityImage = convolve(image, idenity)
sharpenImage = convolve(image, sharpen)
boxBlurImage = convolve(image, boxBlur)
gaussianBlurImage = convolve(image, gaussianBlur)

cv.imwrite("report/problem2/edgeDetectionImage1.bmp", edgeDetectionImage1)
cv.imwrite("report/problem2/edgeDetectionImage2.bmp", edgeDetectionImage2)
cv.imwrite("report/problem2/edgeDetectionImage3.bmp", edgeDetectionImage3)
cv.imwrite("report/problem2/idenityImage.bmp", idenityImage)
cv.imwrite("report/problem2/sharpenImage.bmp", sharpenImage)
cv.imwrite("report/problem2/boxBlurImage.bmp", boxBlurImage)
cv.imwrite("report/problem2/gaussianBlurImage.bmp", gaussianBlurImage)
```
![Edge Detection](problem2/edgeDetectionImage1.bmp)

![Edge Detection](problem2/edgeDetectionImage2.bmp)

![Edge Detection](problem2/edgeDetectionImage3.bmp)

![Sharpen](problem2/sharpenImage.bmp)

![Idenity](problem2/idenityImage.bmp)

![Gaussian Blur](problem2/gaussianBlurImage.bmp)

![Box Blur](problem2/boxBlurImage.bmp)

### Problem 3

**Invert**

```python
import os
import cv2 as cv

images = os.listdir("D:/Dev/OSIRV/osirv_20_21/lab2/slike")

for imageName in images:
    imageName = imageName.split(".")[0]
    image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/" + imageName + ".bmp", 0)
    cv.imwrite("report/problem3/" + imageName + "_invert.bmp", cv.bitwise_not(image))
```

![Slika 1](problem3/airplane_invert.bmp)

![Slika 2](problem3/baboon_invert.bmp)

![Slika 3](problem3/boats_invert.bmp)

![Slika 4](problem3/barbara_invert.bmp)


### Problem 4

**Threshold**

```python
import os
import cv2 as cv

images = os.listdir("D:/Dev/OSIRV/osirv_20_21/lab2/slike")

thresholds = [63, 127, 191]

for imageName in images:
    imageName = imageName.split(".")[0]
    image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/" + imageName + ".bmp", 0)
    for threshold in thresholds:
        treshImage = image.copy()
        ret,treshImage = cv.threshold(treshImage, threshold, 255, type = cv.THRESH_TOZERO)
        cv.imwrite("report/problem4/"+ imageName + "_" + str(threshold) + "thresh.bmp", treshImage)
```

![Slika 63 Threshold](problem4/airplane_63thres.bmp)

![Slika 127 Threshold](problem4/airplane_127thres.bmp)

![Slika 191 Threshold](problem4/airplane_191thres.bmp)


### Problem 5

**Quantisation**


```python
import cv2 as cv
import numpy as np

def quantisation(image, d):
    image = image.astype(np.float32)
    image = (np.floor(image/d) + 0.5) * d
    image[image > 255] = 255
    image[image < 0] = 0
    image = image.astype(np.uint8)
    return image
    

image = cv.imread("slike/BoatsColor.bmp", 0)
image = np.array(image)


for q in range(1, 9):
    d = 2**(8-q)
    qimage = quantisation(image, d)
    cv.imwrite("report/problem5/boat_" + str(q) + ".bmp", qimage)


#In first four images where q has values 8 to 5, quality is on same level. Images have good quality. 
#At q=4 there is some visible deterioration in image quality, mostly top part of the image
#With lower q values detorioration in quality is more visible.
```
![Q = 1](problem5/boat_1.bmp)

![Q = 2](problem5/boat_2.bmp)

![Q = 3](problem5/boat_3.bmp)

![Q = 4](problem5/boat_4.bmp)

![Q = 5](problem5/boat_5.bmp)

![Q = 6](problem5/boat_6.bmp)

![Q = 7](problem5/boat_7.bmp)

![Q = 8](problem5/boat_8.bmp)

### Problem 6

**Noise**

```python
import cv2 as cv
import numpy as np

def noise(image, d):
    n = np.random.uniform(0,1, image.shape)
    image = image.astype(np.float32)
    image = (np.floor((image/d) + n) + 0.5) * d
    image[image > 255] = 255
    image[image < 0] = 0
    image = image.astype(np.uint8)
    return image

image = cv.imread("slike/BoatsColor.bmp", 0)
image = np.array(image)

for q in range(1, 9):
    d = 2**(8-q)
    qimage = noise(image, d)
    cv.imwrite("report/problem6/boat_" + str(q) + "n.bmp", qimage)


#In first four images where q has values 8 to 5, have no visible noise. 
#At q=4 there is some noise, which is visible mostly on clouds.
#With lower q values noise is more noticeable.
```
![Q = 1](problem6/boat_n1.bmp)

![Q = 2](problem6/boat_n2.bmp)

![Q = 3](problem6/boat_n3.bmp)

![Q = 4](problem6/boat_n4.bmp)

![Q = 5](problem6/boat_n5.bmp)

![Q = 6](problem6/boat_n6.bmp)

![Q = 7](problem6/boat_n7.bmp)

![Q = 8](problem6/boat_n8.bmp)


### Problem 7

**Rotacija**

```python
import cv2 as cv
import numpy as np

image = cv.imread('slike/baboon.bmp', 0)
image = cv.resize(image,None,fx=0.25, fy=0.25)
rows,cols = image.shape

rotatedImage = image.copy()
for i in range(1, 12):  
    M = cv.getRotationMatrix2D((cols/2,rows/2),30 * i,1) 
    dst = cv.warpAffine(image,M,(cols,rows))
    rotatedImage = np.hstack((rotatedImage, dst))

cv.imwrite("report/problem7/rotatedBaboon.bmp", rotatedImage)
```
![Slika](problem7/rotatedBaboon.bmp)




