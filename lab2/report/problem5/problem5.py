import cv2 as cv
import numpy as np

def quantisation(image, d):
    image = image.astype(np.float32)
    image = (np.floor(image/d) + 0.5) * d
    image[image > 255] = 255
    image[image < 0] = 0
    image = image.astype(np.uint8)
    return image
    

image = cv.imread("slike/BoatsColor.bmp", 0)
image = np.array(image)


for q in range(1, 9):
    d = 2**(8-q)
    qimage = quantisation(image, d)
    cv.imwrite("report/problem5/boat_" + str(q) + ".bmp", qimage)


#In first four images where q has values 8 to 5, quality is on same level. Images have good quality. 
#At q=4 there is some visible deterioration in image quality, mostly top part of the image
#With lower q values detorioration in quality is more visible.