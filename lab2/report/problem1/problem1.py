import numpy as np
import cv2 as cv

def makeImageBorder(bordersize, image):

    height = image.shape[0]
    width = image.shape[1]

    borderImage = np.hstack((np.zeros((height, bordersize, 3), dtype = np.uint8), image, np.zeros((height, bordersize, 3), dtype = np.uint8)))
    
    borderImage = np.vstack((np.zeros((bordersize, width + 2*bordersize, 3), dtype=np.uint8), borderImage, np.zeros((bordersize, width + 2*bordersize, 3), dtype = np.uint8)))

    return borderImage


imageWidth = 300
imageHeight = 300

image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/baboon.bmp")
image1 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/baboon.bmp")
image2 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/lenna.bmp")
image3 = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/airplane.bmp")

rImage1 = cv.resize(image1, (imageWidth, imageHeight))
rImage2 = cv.resize(image2, (imageWidth, imageHeight))
rImage2 = cv.resize(image3, (imageWidth, imageHeight))

mergedImage = np.hstack((rImage1, rImage2, rImage2))

borderImage = makeImageBorder(20, image)

cv.imshow("Merged image", mergedImage)
cv.imshow("Image with border", borderImage)
cv.imwrite("report\problem1\merged.bmp", mergedImage)
cv.imwrite("report\problem1\\border.bmp", borderImage)

cv.waitKey(0)
cv.destroyAllWindows()
