import os
import cv2 as cv

images = os.listdir("D:/Dev/OSIRV/osirv_20_21/lab2/slike")

thresholds = [63, 127, 191]

for imageName in images:
    imageName = imageName.split(".")[0]
    image = cv.imread("D:/Dev/OSIRV/osirv_20_21/lab2/slike/" + imageName + ".bmp", 0)
    for threshold in thresholds:
        treshImage = image.copy()
        ret,treshImage = cv.threshold(treshImage, threshold, 255, type = cv.THRESH_TOZERO)
        cv.imwrite("report/problem4/"+ imageName + "_" + str(threshold) + "thresh.bmp", treshImage)