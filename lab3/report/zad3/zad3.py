import cv2
import numpy as np
from matplotlib import pyplot as plt

boat = cv2.imread("slike/boats.bmp",0)
baboon = cv2.imread("slike/baboon.bmp", 0)
airplane = cv2.imread("slike/airplane.bmp", 0)
images = [boat, baboon, airplane]
imageTitles = ["boats", "baboon", "airplane"]
thresholds = [56, 79, 100]


for i in range(len(images)):
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_ORIGINAL.jpg", images[i])
    for threshold in thresholds:
        ret,thresh1 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY)
        ret,thresh2 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY_INV)
        ret,thresh3 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TRUNC)
        ret,thresh4 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO)
        ret,thresh5 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO_INV)

        titles = ['BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
        threshImages = [thresh1, thresh2, thresh3, thresh4, thresh5]
        

        for j in range(len(threshImages)):
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_" + titles[j] + "_thresh_" + str(threshold) + ".jpg", threshImages[j])


for i in range(len(images)):
    thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    ret,thresh8 = cv2.threshold(images[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    titles = ['ADAPTIVE_MEAN', 'ADAPTIVE_GAUSSIAN', 'OTSU']
    threshImages = [thresh6,thresh7, thresh8]

    for j in range(len(threshImages)):
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_" + titles[j] + ".jpg", threshImages[j])