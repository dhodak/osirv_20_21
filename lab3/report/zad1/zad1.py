import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def savehist(img, filename):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(filename)

img = cv2.imread("slike/airplane.bmp",0)

sigmas = [1, 5, 10, 20, 40, 60]
uniforms = [20, 40, 60]
saltPepper = [5, 10, 15, 20]


for sigma in sigmas:
    gaussianImg = gaussian_noise(img, 0, sigma)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_gaussian_sigma" + str(sigma) + ".bmp", gaussianImg)
    savehist(gaussianImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_gaussian_sigma" + str(sigma) + ".jpg")

for value in uniforms:
    uniformImg = gaussian_noise(img, -value, value)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_uniform_noise(" + str(-value) + "," + str(value) + ").bmp", uniformImg)
    savehist(uniformImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_uniform_noise(" + str(-value) + "," + str(value) + ").jpg")

for percent in saltPepper:
    saltPepperImg = salt_n_pepper_noise(img, percent)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_saltPepper_percent" + str(percent) + ".bmp", saltPepperImg)
    savehist(saltPepperImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_saltPepper_percent" + str(percent) + ".jpg")
