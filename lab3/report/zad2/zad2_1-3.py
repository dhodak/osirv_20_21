import numpy as np
import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
  
boats = cv2.imread('slike/boats.bmp', 0)
airplane = cv2.imread('slike/airplane.bmp', 0)

images = [boats, airplane]
images_names = ["boats", "airplane"]
sigmas = [5, 15, 35]
saltPepper = [1, 10]
neighborhoods = [3, 5, 7, 9]
gaussianKernels = [3, 5, 7]
gaussianBlurSigmas = [5, 15, 35]

i = 0
for image in images:
    for sigma in sigmas:
        gaussianImg = gaussian_noise(image, 0, sigma)
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + ".jpg", gaussianImg)
        for neighborhood in neighborhoods:
            median = cv2.medianBlur(gaussianImg, neighborhood)
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + "_median" + str(neighborhood) + ".jpg", median)
        for kernel in gaussianKernels:
            for blurSigma in gaussianBlurSigmas:
                blur = cv2.GaussianBlur(gaussianImg, (kernel, kernel), blurSigma)
                cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + "_blurSigma" + str(blurSigma) + "_kernel"+ str(kernel) + ".jpg", blur)


    for percent in saltPepper:
        saltPepperImg = salt_n_pepper_noise(image, percent)
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + ".jpg", saltPepperImg)
        for neighborhood in neighborhoods:
            median = cv2.medianBlur(saltPepperImg, neighborhood)
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + "_median" + str(neighborhood) + ".jpg", median)
        for kernel in gaussianKernels:
            for blurSigma in gaussianBlurSigmas:
                blur = cv2.GaussianBlur(saltPepperImg, (kernel, kernel), blurSigma)
                cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + "_blurSigma" + str(blurSigma) + "_kernel"+ str(kernel) + ".jpg", blur)
    i = i+1