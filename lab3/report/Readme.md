### Zad 1

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def savehist(img, filename):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(filename)

img = cv2.imread("slike/airplane.bmp",0)

sigmas = [1, 5, 10, 20, 40, 60]
uniforms = [20, 40, 60]
saltPepper = [5, 10, 15, 20]


for sigma in sigmas:
    gaussianImg = gaussian_noise(img, 0, sigma)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_gaussian_sigma" + str(sigma) + ".bmp", gaussianImg)
    savehist(gaussianImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_gaussian_sigma" + str(sigma) + ".jpg")

for value in uniforms:
    uniformImg = gaussian_noise(img, -value, value)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_uniform_noise(" + str(-value) + "," + str(value) + ").bmp", uniformImg)
    savehist(uniformImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_uniform_noise(" + str(-value) + "," + str(value) + ").jpg")

for percent in saltPepper:
    saltPepperImg = salt_n_pepper_noise(img, percent)
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_saltPepper_percent" + str(percent) + ".bmp", saltPepperImg)
    savehist(saltPepperImg, "D:/Dev/OSIRV/osirv_20_21/lab3/report/zad1/airplane_hist_saltPepper_percent" + str(percent) + ".jpg")
```
[Zad1 Slike](zad1)

Slike su označene po metodama primjene šumova s određenim parametrima te postoje i slike histograma za tu metodu i parametre. 
Histogramom se može primjetiti utjecaj promjene paramatera kod primjene metoda šumova. Primjerice kod povećanja sigme u gaussian noise metodi,
pojavljuje se veći broj bijelih tonova. 


### Zad 2

```python
import numpy as np
import cv2

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)
  
boats = cv2.imread('slike/boats.bmp', 0)
airplane = cv2.imread('slike/airplane.bmp', 0)

images = [boats, airplane]
images_names = ["boats", "airplane"]
sigmas = [5, 15, 35]
saltPepper = [1, 10]
neighborhoods = [3, 5, 7, 9]
gaussianKernels = [3, 5, 7]
gaussianBlurSigmas = [5, 15, 35]

i = 0
for image in images:
    for sigma in sigmas:
        gaussianImg = gaussian_noise(image, 0, sigma)
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + ".jpg", gaussianImg)
        for neighborhood in neighborhoods:
            median = cv2.medianBlur(gaussianImg, neighborhood)
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + "_median" + str(neighborhood)\ 
            + ".jpg", median)
        for kernel in gaussianKernels:
            for blurSigma in gaussianBlurSigmas:
                blur = cv2.GaussianBlur(gaussianImg, (kernel, kernel), blurSigma)
                cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_gaussian_sigma" + str(sigma) + "_blurSigma" + str
                (blurSigma) + "_kernel"+ str(kernel) + ".jpg", blur)


    for percent in saltPepper:
        saltPepperImg = salt_n_pepper_noise(image, percent)
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + ".jpg", saltPepperImg)
        for neighborhood in neighborhoods:
            median = cv2.medianBlur(saltPepperImg, neighborhood)
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + "_median" + str(neighborhood) +\ 
            ".jpg", median)
        for kernel in gaussianKernels:
            for blurSigma in gaussianBlurSigmas:
                blur = cv2.GaussianBlur(saltPepperImg, (kernel, kernel), blurSigma)
                cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad2/"+ images_names[i] +"_saltPepper" + str(percent) + "_blurSigma" + str\
                (blurSigma) + "_kernel"+ str(kernel) + ".jpg", blur)
    i = i+1
```

[Zad2 Slike](zad2)

Slike su označene po metodama primjene šumova s određenim parametrima te metodama za uklanjanje šumova s određenim parametrima.
Možemo primjetiti da primjena Gaussovog filtra bolje uklanja šum kod Gaussovog šuma, a median filter bolji je za salt and pepper šum. 


### Zad 3

```python
import cv2
import numpy as np
from matplotlib import pyplot as plt

boat = cv2.imread("slike/boats.bmp",0)
baboon = cv2.imread("slike/baboon.bmp", 0)
airplane = cv2.imread("slike/airplane.bmp", 0)
images = [boat, baboon, airplane]
imageTitles = ["boats", "baboon", "airplane"]
thresholds = [56, 79, 100]


for i in range(len(images)):
    cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_ORIGINAL.jpg", images[i])
    for threshold in thresholds:
        ret,thresh1 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY)
        ret,thresh2 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY_INV)
        ret,thresh3 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TRUNC)
        ret,thresh4 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO)
        ret,thresh5 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO_INV)

        titles = ['BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
        treshImages = [thresh1, thresh2, thresh3, thresh4, thresh5]
        

        for j in range(len(treshImages)):
            cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_" + titles[j] + "_thresh_" + str(threshold) + ".jpg",\
            treshImages[j])

for i in range(len(images)):
    thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    ret2,thresh8 = cv2.threshold(images[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    titles = ['ADAPTIVE_MEAN', 'ADAPTIVE_GAUSSIAN', 'OTSU']
    threshImages = [thresh6,thresh7, thresh8]

    for j in range(len(threshImages)):
        cv2.imwrite("D:/Dev/OSIRV/osirv_20_21/lab3/report/zad3/" + imageTitles[i] + "_" + titles[j] + ".jpg", treshImages[j])
```

[Zad3 Slike](zad3)

Slike su označene po metodama primjene thresholda s određenim threshold parametrima te metodama.
Binarna metoda zahtjeva korekciju parametara te u ovom slučaju OSTU daje bolju sliku. 
Adaptive mean i adaptive gaussian ističu rubove na slikama gdje je mean metoda nešto oštrija.
Ostale metode ovise svjetlini slika i threshold parametru kojeg sam zadao na 56, 79, 100 te u konačnoj usporedbi možemo zaključiti
da adaptive metode definitivno daju bolje rezulatate za slike s različitim intezitetima svjetlin što je i očekivano.
