import cv2

captureDevice = cv2.VideoCapture(0, cv2.CAP_DSHOW)

while True:
    ret, frame = captureDevice.read()

    cv2.imshow('my frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('a'):
        break

captureDevice.release()
cv2.destroyAllWindows()
