# Lab 6: Image Segmentation using Watershed

In this lab, you'll implement a full watershed segmentation pipeline for segmenting blood cells from a microscope blood smear image.

![](images/cells.jpg)

Once you're done, you'll obtain an image similar to the following:

![](images/result_example.png)

Each color represents one class of segmented pixels. The cells in the image as well as the image background are roughly segmented in the image. The image also shows areas of **undersegmentation** and **oversegmentation**. Undersegmentation happens when an area of the image contains multiple classes in reality, but your algorithm shows only one class. This usually happens when two objects overlap and the algorithm treats them as one large object. Oversegmentation happens when a single object is segmented into multiple classes by your algorithm.

By obtaining an image like the one above we crop out each cell on the image for further processing steps (like classifying into white and red blood cells, for instance), we can count the cells, analyze their density, size, shapes, etc. which can give us automatic diagnostic information to use in studies or clinical practice.

## Watershed segmentation

Very simple segmentation methods like thresholding sometimes give good results, but as soon as we have more complex images, we need more complex algorithms. One problem that thresholding can't solve is to segment overlapping objects. In the cell image above, you'll notice a lot of the cells overlap and look like they are joined. To correctly segment them, we need an algorithm that is capable of separating overlapping objects.

One such algorithm is **watershed segmentation**. To understand watershed, think of a grayscale image as a 3D topology map, where the value of the pixel is the height, as shown in the bottom-left of the following image:

![](watershed_1.png)

_(source: https://www.youtube.com/watch?v=jLd2I2adQtw)_

Now imagine pouring water over the image. As the basins fill with water, neighboring water bodies will touch. That point of contact forms lines around objects called a ridgeline. These lines are then used to segment one object from another.

Watershed also sometimes include **markers**. These are pixels for which we are certain belong to different objects, shown on the above image as red points. By giving watershed markers, we can make sure it finds the correct number of objects.

![](watershed_2.png)

_(source: https://www.youtube.com/watch?v=WQpXS9gBEu8)_

Because of this water-filling approach, watershed can segment two objects even if they overlap in the image.

## Distance transform as preprocessing for watershed

To use watershed, you first need an image that you can treat as a smooth 3D topology map to pour water in. You can't use the original microscope image, instead, you need to convert it into a grayscale image where cells have light values and the background is black. To do this you'll use **distance transform**. Distance transform of a binary image (with pixel values 1 or 0) will give you an image where each pixel's value is the distance to its closest edge.

![](distance_transform.jpg)

*(source: https://www.researchgate.net/publication/220998362_A_New_Image_Quality_Measure_Considering_Perceptual_Information_and_Local_Spatial_Feature/figures?lo=1)*

This way you transform a binary image into a smooth image where the distance to the edge is the "height" of the mountain.

## CLAHE

Contrast Limited Adaptive Histogram Equalization or CLAHE is a technique for histogram equalization, improving the contrast of the image. Bright images have only light pixel values, while dark images have only dark pixel values. Histogram equalization stretches the histogram of an image so that its values span a broader range, making both dark and bright images have the same mean pixel value.

You'll use CLAHE as a preprocessing step for your algorithm. More info: https://docs.opencv.org/master/d5/daf/tutorial_py_histogram_equalization.html

## Segmentation flow

So, to segment your image, here's what you'll do:

1. Convert the microscope image into grayscale.
2. Perform CLAHE on the grayscale image to accentuate cell edges.
3. Threshold the image so you get a binary image where cells are white and the background is black.
4. Use morphological operators to remove noise from that binary image.
5. Use distance transform on the binary image.
6. Threshold the distance transform to obtain markers.
7. Apply watershed with the obtained markers to get the final segmentation.

## Reading the OpenCV docs

To use OpenCV, you'll often have to refer to OpenCV documentation. At first, it can be hard to know exactly  how to use a function when you read the docs, so here are a few tips.

For example, open https://docs.opencv.org/3.4/d2/de8/group__core__array.html#ga0002cf8b418479f4cb49a75442baee2f

This is the documentation for the `bitwise_not` function. Take a look at the **Python** section to see the method signature and how you would call it. It looks like this:

```
Python:
	dst	=	cv.bitwise_not(	src[, dst[, mask]]	)
```

This is a function that's called `bitwise_not` and is called on the OpenCV object called `cv`. By default, if you use `import cv2` in your file, this object will be called `cv2`. However, you can use `import cv2 as cv` and then the object will be called `cv`.

Generally, `[` and `]` signify that that part of the text is **optional**. In other words, `src` is **required** for this function, but you don't need to write `dst` or `mask`. This means you can call this function in four different ways:

```swift
dst	=	cv.bitwise_not(src)
dst = cv.bitwise_not(src, dst)
dst = cv.bitwise_not(src, dst, mask)
dst = cv.bitwise_not(src, mask=my_mask)
```

Below the function signature, is a description of what the function that as well as each parameter of the function. 

Sometimes functions will take constant values, for instance the [`cv.imread()`](https://docs.opencv.org/3.4/d4/da8/group__imgcodecs.html#ga288b8b3da0892bd651fce07b3bbd3a56) function takes a `flags` parameter which tells OpenCV how to read the image. You can find the values of these flags by clicking the type of the parameter:

https://docs.opencv.org/3.4/d8/d6a/group__imgcodecs__flags.html#gga61d9b0126a3e57d9277ac48327799c80af660544735200cbe942eea09232eb822 

## Assignments

1. Open **main.py** and get familiar with the code. Your assignment is to fill in code in the **three** `# TODO` comments in the code. The code will perform segmentation and save images. Compare your saved images with the ones named `<image>_example.png` to make sure your code works.
2. Rename your **result.png** image into **result_original.png**. Then, comment out `gray = clahe(gray)` in **main.py**. Run the code again and compare the new **result.png** to your original one. Add a comment at the bottom of **main.py** explaining what differences you see and why those differences are there.
3. Annotate your result image however you like (using any image editing tool) to show (1) one example of **oversegmentation** on the image and (2) one example of **undersegmentation** on the image.