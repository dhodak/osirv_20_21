import cv2

def image_detect_and_compute(detector, img_name):
    """Detect and compute intetrest points and their descriptors."""
    img = cv2.imread(img_name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des


def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    """Draw ORB feature matches of the given two images."""
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches],img2, flags=2)

    cv2.imwrite('imageMatches.jpg', img_matches)



img = cv2.imread('../../slike/roma_1.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
orb = cv2.ORB_create()

key_points, description = orb.detectAndCompute(img, None)
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.imwrite('ORBdetector.jpg', img_keypoints)

orb = cv2.ORB_create()
draw_image_matches(orb, '../../slike/roma_1.jpg', '../../slike/roma_2.jpg')


