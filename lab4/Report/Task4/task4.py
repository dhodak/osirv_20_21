import join
import cv2
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')

img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

edgedImgs = []

for blocksize in [1,3,5]:
    img3 = copy.copy(img2)

    dst = cv2.cornerHarris(gray,blocksize,3,0.04)

    dst = cv2.dilate(dst,None)

    img3[dst>0.01*dst.max()]=[0,0,255]

    edgedImgs.append(img3)

stackImges = join.stackImages(0.7, edgedImgs)

cv2.imwrite('harrisCornerDetection.jpg', stackImges)

