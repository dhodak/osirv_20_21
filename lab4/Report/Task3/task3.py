import cv2
import math
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')

img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)
theta = [90, 180]
thresh = [150, 200]

for tht in theta:
    for thr in thresh:
        lines = cv2.HoughLines(edges, 1, math.pi / tht, thr, np.array([]), 0, 0)
        a, b, c = lines.shape
        for i in range(a):
            img3 = copy.copy(img2)
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = (int(x0+1000*(-b)), int(y0+1000*a))
            pt2 = (int(x0-1000*(-b)), int(y0-1000*a))
            cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

        cv2.imwrite('chess_theta' + str(tht) + '_threshold' + str(thr) + '.jpg', img3)



