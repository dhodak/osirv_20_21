import cv2
import join

path = 'D:\Projects\osirv_20_21\lab4\slike\lenna.bmp'

img = cv2.imread(path, 0)
blurred = cv2.GaussianBlur(img, (5,5), 0)
lowerValues = [0, 128, 255]
upperValues = [255, 128, 255]

edgedImgs = []


for i in range(3):
    edgedImgs.append(cv2.Canny(blurred, lowerValues[i], upperValues[i]))


imgStack = join.stackImages(0.7, edgedImgs)

cv2.imwrite('edgeDetection.jpg', imgStack)
