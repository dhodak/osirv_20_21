import cv2
import numpy as np
import join

def auto_canny(image, sigma=0.33):
    v = np.median(image)

    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    print("Median lower: %r." % lower)
    print("Median upper: %r." % upper)

    return edged


airplane = cv2.imread('../../slike/airplane.bmp', 0)
barbara = cv2.imread('../../slike/barbara.bmp', 0)
boats = cv2.imread('../../slike/boats.bmp', 0)
pepper = cv2.imread('../../slike/pepper.bmp', 0)

edgedImgs = join.stackImages(0.7, ([auto_canny(airplane), auto_canny(barbara), auto_canny(boats), auto_canny(pepper)]))

cv2.imwrite('automaticEdgeDetection.jpg', edgedImgs)

