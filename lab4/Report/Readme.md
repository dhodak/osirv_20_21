### Task 1

```python
import cv2
import join

path = 'D:\Projects\osirv_20_21\lab4\slike\lenna.bmp'

img = cv2.imread(path, 0)
blurred = cv2.GaussianBlur(img, (5,5), 0)
lowerValues = [0, 128, 255]
upperValues = [255, 128, 255]

edgedImgs = []


for i in range(3):
    edgedImgs.append(cv2.Canny(blurred, lowerValues[i], upperValues[i]))


imgStack = join.stackImages(0.7, edgedImgs)

cv2.imwrite('edgeDetection.jpg', imgStack)
```

Lower = 0, Upper = 255 -> Lower = 128, Upper = 128 -> Lower = 255, Upper = 255
![Canny Detection](Task1/edgeDetection.jpg)

Optimalna vrijednost bi bila srednja vrijednost gdje su donji i gornji pragovi na 128. 


### Task 2

```python
import cv2
import numpy as np
import join

def auto_canny(image, sigma=0.33):
    v = np.median(image)

    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    print("Median lower: %r." % lower)
    print("Median upper: %r." % upper)

    return edged


airplane = cv2.imread('../../slike/airplane.bmp', 0)
barbara = cv2.imread('../../slike/barbara.bmp', 0)
boats = cv2.imread('../../slike/boats.bmp', 0)
pepper = cv2.imread('../../slike/pepper.bmp', 0)

edgedImgs = join.stackImages(0.7, ([auto_canny(airplane), auto_canny(barbara), auto_canny(boats), auto_canny(pepper)]))

cv2.imwrite('automaticEdgeDetection.jpg', edgedImgs)
```

![Automatic Canny Detection](Task2/automaticEdgeDetection.jpg)

### Task 3

```python
import cv2
import math
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')

img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)
theta = [90, 180]
thresh = [150, 200]

for tht in theta:
    for thr in thresh:
        lines = cv2.HoughLines(edges, 1, math.pi / tht, thr, np.array([]), 0, 0)
        a, b, c = lines.shape
        for i in range(a):
            img3 = copy.copy(img2)
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = (int(x0+1000*(-b)), int(y0+1000*a))
            pt2 = (int(x0-1000*(-b)), int(y0-1000*a))
            cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

        cv2.imwrite('chess_theta' + str(tht) + '_threshold' + str(thr) + '.jpg', img3)
```

![Theta 90 Thresh 150](Task3/chess_theta90_threshold150.jpg)
![Theta 90 Thresh 200](Task3/chess_theta90_threshold200.jpg)
![Theta 180 Thresh 150](Task3/chess_theta180_threshold150.jpg)
![Theta 180 Thresh 150](Task3/chess_theta180_threshold150.jpg)


### Task 4

```python
import join
import cv2
import numpy as np
import copy

img = cv2.imread('../../slike/chess.jpg')

img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

edgedImgs = []

for blocksize in [1,3,5]:
    img3 = copy.copy(img2)

    dst = cv2.cornerHarris(gray,blocksize,3,0.04)

    dst = cv2.dilate(dst,None)

    img3[dst>0.01*dst.max()]=[0,0,255]

    edgedImgs.append(img3)

stackImges = join.stackImages(0.7, edgedImgs)

cv2.imwrite('harrisCornerDetection.jpg', stackImges)
```

Blocksize = 1 -> Blocksize = 3 -> Blocksize = 5

![Harris](Task4/harrisCornerDetection.jpg)

Najbolji rezultat daju blocksize 3 i 5, gdje blocksize 5 daje nešto više kuteva.

### Task 5

```python
import cv2

def image_detect_and_compute(detector, img_name):
    """Detect and compute intetrest points and their descriptors."""
    img = cv2.imread(img_name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des


def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    """Draw ORB feature matches of the given two images."""
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches],img2, flags=2)

    cv2.imwrite('imageMatches.jpg', img_matches)



img = cv2.imread('../../slike/roma_1.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
orb = cv2.ORB_create()

key_points, description = orb.detectAndCompute(img, None)
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.imwrite('ORBdetector.jpg', img_keypoints)

orb = cv2.ORB_create()
draw_image_matches(orb, '../../slike/roma_1.jpg', '../../slike/roma_2.jpg')
```

![ORB detector](Task5/ORBdetector.jpg)

![Matches](Task5/imageMatches.jpg)