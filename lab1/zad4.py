import numpy
import cv2

image = cv2.imread('slike/baboon.bmp')

b = image.copy()
b[:, :, 1] = 0
b[:, :, 2] = 0


g = image.copy()
g[:, :, 0] = 0
g[:, :, 2] = 0

r = image.copy()
r[:, :, 0] = 0
r[:, :, 1] = 0


cv2.imshow('Blue', b)
cv2.imshow('Green', g)
cv2.imshow('Red', r)

cv2.waitKey(0)

