
def removeSpaces(text):
    return text.replace(" ", "")

def maxLength(values):
    
    string_length = 0
    
    for value in values:
        length = len(removeSpaces(value))
      
        if length > string_length:
            string_length = length
            text = value

    return text       
            
        
print("Enter a street name or word Done if you want to stop a loop")

street_names = []

while(1):
    street_name = input("Enter:")

    if(street_name == "Done"):
        break

    if (len(removeSpaces(street_name)) < 7) or (len(removeSpaces(street_name)) > 15):
        print("Street name must be between 7 and 15 characters")
        continue

    street_names.append(street_name)

longest_string = maxLength(street_names)

print("Street with the most characters is %s (%d)" % (longest_string, len(removeSpaces(longest_string))))
print("Number of streets is %d" % len(street_names))
