import numpy
import cv2


image = cv2.imread("slike/baboon.bmp")

vertical = image.copy()
vertical = vertical[::2]

horizontal = image.copy()
horizontal = horizontal[:, ::2]

vert_horiz = image.copy()
vert_horiz = vert_horiz[::2, ::2]


cv2.imshow('vertical', vertical)
cv2.imshow('horizontal', horizontal)
cv2.imshow('vertical and horizontal', vert_horiz)
cv2.waitKey(0)

cv2.imwrite("slike/vertical.bmp", vertical)
cv2.imwrite("slike/horizontal.bmp", horizontal)
cv2.imwrite("slike/verticalhorizonal.bmp", vert_horiz)