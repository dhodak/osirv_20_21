import numpy
import cv2

im = cv2.imread("slike/barbara.bmp")

bordersize = 10
border = cv2.copyMakeBorder(
    im,
    top=bordersize,
    bottom=bordersize,
    left=bordersize,
    right=bordersize,
    borderType=cv2.BORDER_CONSTANT,
    value=[255, 255, 255]
)


cv2.imshow('border', border)
cv2.waitKey(0)

cv2.imwrite("slike/border.jpg", border)