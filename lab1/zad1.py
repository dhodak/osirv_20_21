import random

random_num = random.randrange(0, 15)
count = 0

while(1):
    try:
        guess = int(input("Guess the random number in interval [0, 15]"))
        if (guess < 0) or (guess > 15):
            print("Not in interval")
            continue

        count += 1

        if guess == random_num:
            print("Nice! Number of attempts is %d" % count)
            break

    except ValueError:
        print("Not a number")
        

