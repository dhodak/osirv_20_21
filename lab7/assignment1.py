import cv2 as cv

alpha_slider_max = 100
hsv_window = "HSV adjust"

img = cv.imread("images/peppers.png")

# This function is called every time the trackbar's value changes.
def on_trackbar(val):
  # TODO: Declare img_hsv with the value of img transformed to HSV
  img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)


  # TODO: Add val the hue value for all pixels of img_hsv 
  # the first column of the img_hsv matrix += val
  img_hsv[:,:,0] += val


  # TODO: Declare img_copy with the value of img_hsv transformed to BGR
  img_copy = cv.cvtColor(img_hsv, cv.COLOR_HSV2BGR)


  cv.imshow(hsv_window, img_copy)

# Create a named window
cv.namedWindow(hsv_window)
# Add a trackbar to that window
cv.createTrackbar("Hue", hsv_window, -100, 100, on_trackbar)
# Initialize and show image for the first time
on_trackbar(0)

cv.waitKey()